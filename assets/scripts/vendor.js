const barraSaludMonstruo = document.getElementById("salud-monstruo");
const barraSaludJugador = document.getElementById("salud-jugador");
const contenedorVidaExtra = document.getElementById("vida-extra");

const btnAtaque = document.getElementById("btn-ataque");
const btnSuperAtaque = document.getElementById("btn-super-ataque");
const btnCurar = document.getElementById("btn-curar");
const btnHistorial = document.getElementById("btn-historial");

/**
 * Establece el valor máximo de puntos de salud y rellena las barras hasta ese valor.
 * @param {number} saludMaxima Máximo número de puntos de salud que puede tener cada jugador
 */
function maximizarBarrasSalud(saludMaxima) {
  barraSaludMonstruo.max = saludMaxima;
  barraSaludMonstruo.value = saludMaxima;
  barraSaludJugador.max = saludMaxima;
  barraSaludJugador.value = saludMaxima;
}

/**
 * Reduce la salud del monstruo en un valor aleatorio entre 1 y dañoMaximo
 * @param {number} dañoMaximo Máximo daño que puede realizar el ataque
 * @returns {number} Daño inflingido al monstruo
 */
function dañarMonstruo(dañoMaximo) {
  const dañoInflingido = Math.ceil(Math.random() * dañoMaximo);
  barraSaludMonstruo.value = +barraSaludMonstruo.value - dañoInflingido;
  return dañoInflingido;
}

/**
 * Reduce la salud del jugador en un valor aleatorio entre 1 y dañoMaximo
 * @param {number} dañoMaximo Máximo daño que puede realizar el ataque
 * @returns {number} Daño inflingido al jugador
 */
function dañarJugador(dañoMaximo) {
  const dañoInflingido = Math.ceil(Math.random() * dañoMaximo);
  barraSaludJugador.value = +barraSaludJugador.value - dañoInflingido;
  return dañoInflingido;
}

/**
 * Incrementa la salud del jugador en el número de puntos que se indican
 * @param {number} salud Salud a recuperar
 */
function aumentarSaludJugador(salud) {
  barraSaludJugador.value = +barraSaludJugador.value + salud;
}

/**
 * Reinicia las barras de salud para una nueva batalla
 * @param {number} salud Salud inicial para la nueva batalla
 */
function reiniciarJuego(salud) {
  barraSaludJugador.value = salud;
  barraSaludMonstruo.value = salud;
}

/**
 * Elimina la vida extra de la interfaz
 */
function eliminarVidaExtra() {
  contenedorVidaExtra.parentNode.removeChild(contenedorVidaExtra);
}

/**
 * Establece la salud del jugador al valor recibido
 * @param {number} salud Salud del jugador
 */
function establecerSaludJugador(salud) {
  barraSaludJugador.value = salud;
}