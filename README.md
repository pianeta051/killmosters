## What is it about

This is a very simple game where we can practice how to interact with the elements of the DOM. We will work with basic functions, using basic concepts of Javascript

1. The machine and the player will start with a life bar of 100%.
2. The player will have a life.
3. The player could attack to the maquine.
4. The machine will attack back with a random force
5. The player can use a super attack and heal in the middle of each turn.
6. You can see the history at all times



---

## What we will use

We will work with pure Javascript without the help of any Framework working with the following:
1. Declaration of variables.
2. Get items from the .js file.
3. Basic function.
4. Insertion and removal of DOM elements.
5. DOM manipulation.
6. Array iteration applying different loops
---

## Clone a repository

Clone the repository and feel free to check out all the code.
Download it by running the index.